package com.example.chatappfirebase;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

class MessageList extends ArrayAdapter {

    private Activity context;
    private List<Message> messageList;

    public MessageList(Activity context, List<Message> messageList) {
        super(context, R.layout.message_layout, messageList);
        this.context = context;
        this.messageList = messageList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.message_layout, null, true);

        TextView textViewAuthor = (TextView) listViewItem.findViewById(R.id.textViewAuthor);
        TextView textViewMessage = (TextView) listViewItem.findViewById(R.id.textViewMessage);
        TextView textViewTime = (TextView) listViewItem.findViewById(R.id.textViewTime);

        Message message = messageList.get(position);

        textViewAuthor.setText(message.getMessageAuthor());
        textViewMessage.setText(message.getMessageText());
        textViewTime.setText(message.getMessageTime());

        return listViewItem;
    }
}
