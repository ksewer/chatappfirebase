package com.example.chatappfirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class ChatActivity extends AppCompatActivity {

    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListener;
    TextView textViewAuthor;
    TextView textViewMessage;
    Button buttonSendMessage;
    EditText editTextMessage;
    ListView listViewMessages;
    TextView textViewTime;
    List<Message> messageList = new ArrayList<>();
    DatabaseReference databaseMessages;
    public static final String CHANNEL_NOTIFICATION_ID = "channel1";
    public static boolean isVibrateOn = false;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

        databaseMessages.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int messageListOldSize = messageList.size();
                messageList.clear();
                for (DataSnapshot artistSnapshot : dataSnapshot.getChildren()) {
                    Message message = artistSnapshot.getValue(Message.class);
                    messageList.add(message);
                }
                if(messageList.size()<messageListOldSize)
                    isVibrateOn = false;

                ArrayAdapter adapter = new MessageList(ChatActivity.this, messageList);
                listViewMessages.setAdapter(adapter);

                if (isVibrateOn)
                    vibrate();
                else
                    isVibrateOn = true;

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void vibrate() {
        int vibrateTime = 150;
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(vibrateTime, VibrationEffect.DEFAULT_AMPLITUDE));
            android.os.SystemClock.sleep(100 + vibrateTime);
            v.vibrate(VibrationEffect.createOneShot(vibrateTime, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(vibrateTime);
            android.os.SystemClock.sleep(100 + vibrateTime);
            v.vibrate(vibrateTime);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mAuth.signOut();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        databaseMessages = FirebaseDatabase.getInstance().getReference("messages");

        textViewAuthor = (TextView) findViewById(R.id.textViewAuthor);
        textViewMessage = (TextView) findViewById(R.id.textViewMessage);
        buttonSendMessage = (Button) findViewById(R.id.buttonSend);
        editTextMessage = (EditText) findViewById(R.id.editTextMessage);
        listViewMessages = (ListView) findViewById(R.id.listViewMessages);
        textViewTime = (TextView) findViewById(R.id.textViewTime);

        listViewMessages.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listViewMessages.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Message message = messageList.get(position);
                databaseMessages.getRoot().child("messages").child(message.getMessageId()).removeValue();
                isVibrateOn=false;

                return true;
            }
        });

        buttonSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    startActivity(new Intent(ChatActivity.this, MainActivity.class));
                }
            }
        };
    }

    private void sendMessage() {
        isVibrateOn = false;
        String author = mAuth.getCurrentUser().getDisplayName();
        String message = editTextMessage.getText().toString().trim();
        String time = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());

        if (!TextUtils.isEmpty(message)) {
            String id = databaseMessages.push().getKey();
            Message messageObject = new Message(id, author, message, time);

            databaseMessages.child(id).setValue(messageObject);
            editTextMessage.setText("");
            Toast.makeText(this, "Wiadomość wysłana!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Nie mogę wysłać pustej wiadomości!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isVibrateOn=false;
    }
}
