package com.example.chatappfirebase;

public class Message {

    String messageId;
    String messageAuthor;
    String messageText;
    String messageTime;

    public Message(String messageId, String messageAuthor, String messageText, String messageTime) {
        this.messageId = messageId;
        this.messageAuthor = messageAuthor;
        this.messageText = messageText;
        this.messageTime = messageTime;
    }

    public Message() {

    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessageAuthor() {
        return messageAuthor;
    }

    public void setMessageAuthor(String messageAuthor) {
        this.messageAuthor = messageAuthor;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
}
